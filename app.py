#!/usr/bin/python3

try:
    import urwid
    from time import sleep
    from sys import stdout
    from subprocess import call
    with open('boot.txt', 'r') as file:
        boot = file.read().strip().split('\n')

    login_banner = (u"Unauthorized access to this computer system and software is prohibited by "
                    "Title 18, United States Code, Section 1030, Fraud and Related Activity in "
                    "Connection with Computers.\n\nThis system is for the use of authorized users "
                    "only. Individuals using this computer system without authority, or in excess "
                    "of their authority, are subject to having all of their activities on this "
                    "system monitored and recorded by system personnel.\n\nIn the course of "
                    "monitoring individuals improperly using this system, or in the case of "
                    "system maintenance, the activities of authorized users may also be monitored.\n"
                    "\nAnyone using this system expressly consents to such monitoring and is advised "
                    "that if such monitoring reveals possible evidence of criminal activity, system "
                    "personnel may provide the evidence of such monitoring to law enforcement "
                    "officials.\n\nUnauthorized attempts to upload information or change information "
                    "on this service are strictly prohibited and may be punishable under the Computer "
                    "Fraud and Abuse Act of 1986 and the National Information Infrastructure Protection "
                    "Act and the Department of Defense Security code 2938.12(b)"
                    )
    final = [
        "Searching for signal",
        "RS-12M No. 1173 identified",
        "Acquiring network link",
        "Negotiating connection",
        "Key sequence accepted",
        "Transmitting new commands"
    ]

    ##
    # Widget helpers
    def prompt(widget_text, swatch=''):
        return urwid.Edit((swatch, u"{}".format(widget_text)))


    def text(widget_text, swatch='', newline=True):
        if newline:
            n = "\n"
        else:
            n = ""
        return urwid.Text((swatch, u"{}{}".format(widget_text, n)))


    def unhandled_input(key):
        if key == 'esc':
            break_main_loop()
    # /widget helpers#
    ##


    ##
    # Misc. helpers
    def break_main_loop():
        raise urwid.ExitMainLoop()


    def slow_print(slow_text, delay):
        for slow_letter in slow_text:
            print(slow_letter, end='')
            stdout.flush()
            sleep(delay)
    # /misc helpers
    ##


    class LoginList(urwid.ListBox):
        def __init__(self):
            body = urwid.SimpleFocusListWalker([prompt('username: ')])
            super(LoginList, self).__init__(body)
            self.input_username = ""
            self.stage = 'username'
            self.tries = 0
            self.lockout = False
            self.n = 99
            with open('password.txt', 'r') as file:
                self.password = file.read()

        def keypress(self, size, key):
            key = super(LoginList, self).keypress(size, key)
            if key in ('up', 'down', 'left', 'right', 'tab') or self.lockout:
                return True
            elif key != 'enter':
                return key
            else:  # key was enter.
                user_input = self.focus.edit_text
                if self.stage == 'username':
                    self.input_username = user_input
                    self.stage = 'password'
                    self.body[0] = urwid.WidgetDisable(self.body[0])
                    self.body.insert(self.focus_position+1, prompt('password: '))
                    self.focus_position += 1
                elif self.stage == 'password':
                    if self.input_username == "StarLight" and user_input == self.password:
                        self.body.insert(
                            self.focus_position+1,
                            text('\nLogin accepted, initializing...', swatch='success')
                        )
                        self.body[1] = urwid.WidgetDisable(self.body[1])
                        loop.set_alarm_in(2, self.password_success_callback)
                    else:
                        self.tries += 1
                        self.body[1] = urwid.WidgetDisable(self.body[1])
                        self.body.insert(
                            self.focus_position+1,
                            text("\nInvalid username or password!", swatch='fail')
                        )
                        self.lockout = True
                        loop.set_alarm_in(1, self.password_fail_callback)

        def password_fail_callback(self, *args):
            if self.tries == 5:
                if self.n == 99:
                    self.n = len(boot)
                    self.body.remove(self.body[2])
                    self.body.remove(self.body[1])
                    self.body.remove(self.body[0])
                    loop.set_alarm_in(0.1, self.password_fail_callback)
                else:
                    if self.n > 0:
                        self.body.insert(
                            len(self.body),
                            text("{}{}".format(boot[(len(boot)-self.n)], "...FAILED"), swatch='fail', newline=False)
                        )
                        self.focus_position = len(self.body)-1
                        loop.set_alarm_in(0.1, self.password_fail_callback)
                        self.n -= 1
                    elif self.n == -1:
                        sleep(1)
                        self.body.insert(
                            len(self.body),
                            text("Module self destructing", swatch='fail2', newline=False)
                        )
                        self.focus_position = len(self.body)-1
                        self.n = -2
                        loop.set_alarm_in(0.1, self.password_fail_callback)
                    else:
                        sleep(10)
                        call('sudo shutdown --poweroff now', shell=True)
            else:
                self.body.remove(self.body[2])
                self.body.remove(self.body[1])
                self.body[0] = prompt('username: ')
                self.stage = 'username'
                self.lockout = False

        def password_success_callback(self, *args):
            if self.n == 99:
                self.n = len(boot)
                self.body.remove(self.body[2])
                self.body.remove(self.body[1])
                self.body.remove(self.body[0])
                loop.set_alarm_in(0.1, self.password_success_callback)
            else:
                if self.n >= 1000:
                    if self.n-1000 < len(final):
                        self.body.insert(
                            len(self.body),
                            text("{}".format(final[self.n-1000]), swatch='success', newline=False)
                        )
                        self.focus_position = len(self.body)-1
                        loop.set_alarm_in(0.5, self.password_success_callback)
                        self.n += 1
                    elif self.n-1000 == len(final):
                        self.body.insert(
                            len(self.body),
                            text("MALFUNCTION", swatch='fail')
                        )
                        self.n = 99999999
                        loop.set_alarm_in(0.1, self.password_success_callback)
                    else:
                        sleep(10)
                        call('sudo shutdown --poweroff now', shell=True)
                elif self.n > 0:
                    self.body.insert(
                        len(self.body),
                        text("{}".format(boot[(len(boot)-self.n)]), swatch='success', newline=False)
                    )
                    self.focus_position = len(self.body)-1
                    loop.set_alarm_in(0.1, self.password_success_callback)
                    self.n -= 1
                else:
                    self.n = 1000
                    self.body[:] = [text("")]
                    self.body.remove(self.body[0])
                    loop.set_alarm_in(0.1, self.password_success_callback)


    class Data:
        def __init__(self):
            self.result = ""

    data = Data()
    palette = [
        ('success', 'light green', ''),
        ('banner', 'dark red', ''),
        ('fail', 'white, blink', 'dark red', ''),
        ('fail2', 'dark red', 'white', ''),
        ('normal', 'white', '', '')
    ]

    header = text(login_banner, swatch='banner')
    frame = urwid.Padding(
        urwid.Frame(
            LoginList(),
            header=header
        ),
        left=1
    )

    view = urwid.LineBox(frame)
    loop = urwid.MainLoop(view, palette, unhandled_input=unhandled_input)
    loop.run()

except KeyboardInterrupt:
    pass
