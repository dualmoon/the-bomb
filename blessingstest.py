#!/usr/bin/python3

from blessings import Terminal
import sys, time
from math import floor


class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()
term = Terminal()
banner = """
 Unauthorized access to this computer system and software is prohibited by
 Title 18, United States Code, Section 1030, Fraud and Related Activity in
 Connection with Computers.

 This system is for the use of authorized users only. Individuals using this
 computer system without authority, or in excess of their authority, are
 subject to having all of their activities on this system monitored and
 recorded by system personnel.

 In the course of monitoring individuals improperly using this system, or in
 the case of system maintenance, the activities of authorized users may also
 be monitored.

 Anyone using this system expressly consents to such monitoring and is
 advised that if such monitoring reveals possible evidence of criminal
 activity, system personnel may provide the evidence of such monitoring to
 law enforcement officials.





"""
strikes = 0


def login_prompt():
    print(term.move(21, 0) +
          term.clear_eos +  # clear rest of screen
          term.green(" username:\n ")
          , end="")
    username = input()
    print(term.green(" password:\n "), end="")
    password = input()
    return username, password


def invalid_login_dialog():
    # put a dialog in the middle of the screen
    with term.hidden_cursor():
        dialog = " " +\
                 term.white_on_red + "┌──────────────────┐" +\
                 term.normal + "\n " +\
                 term.white_on_red + "│  INVALID  LOGIN  │" +\
                 term.normal + "\n " +\
                 term.white_on_red + "└──────────────────┘"
        top = 18  # floor((term.height / 2))
        print(term.move(top, 0) +
              dialog +
              term.normal
              )
        time.sleep(2)
        print(term.move(top, 0) +
              term.clear_eos +
              term.move(top+1, 0) +
              " {}".format(term.white_on_red('X')*strikes) +
              term.normal +
              "{}".format('O'*(5-strikes))
              )

with term.fullscreen():
    print(term.clear() +
          term.red(banner)
          )
    username, password = login_prompt()
    while True:
        if username == "StarLight" and password == "test":
            print("access granted.")
            time.sleep(2)
            break
        else:
            strikes += 1
            if strikes < 5:
                invalid_login_dialog()
                username, password = login_prompt()
            else:
                print(term.white_on_red("ACCESS DENIED"))
                time.sleep(2)
                break
    """
    while True:
        c = getch()
        if c == "q":
            break
        elif c:
            print(term.move(12, 12) +
                  "key pressed: {}".format(c), end=""
                  )
            sys.stdout.flush()
    """
